package com.connect_4.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.connect_4.model.security.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);
}
