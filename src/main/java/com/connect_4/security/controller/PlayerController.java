package com.connect_4.security.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.connect_4.login.dto.PageDataDTO;
import com.connect_4.login.dto.PlayGame;
import com.connect_4.login.dto.SetOpponentPlayer;
import com.connect_4.login.dto.SignUpDTO;
import com.connect_4.login.service.LoginService;
import com.connect_4.security.JwtTokenUtil;
import com.connect_4.security.JwtUser;

@RestController
public class PlayerController {

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "public/signUp", method = RequestMethod.POST)
	public ResponseEntity<?> companySignUp(HttpServletRequest request, @Valid @RequestBody SignUpDTO jso, BindingResult result) {
		if (result.hasErrors()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result.getAllErrors());
		}
		String json;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(jso);
		} catch (IOException e) {
			return (ResponseEntity<?>) ResponseEntity.badRequest();
		}
		return ResponseEntity.ok(loginService.signUp(json));
	}

	@RequestMapping(value = "getOnlinePlayersList", method = RequestMethod.POST)
	public ResponseEntity<?> getOnlinePlayers(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		JwtUser user = (JwtUser) userDetails;
		Integer userId = jwtTokenUtil.getUserIdFromToken(token);
		PageDataDTO jso = new PageDataDTO();
		jso.setUser_id(userId);
		String json;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(jso);
		} catch (IOException e) {
			return (ResponseEntity<?>) ResponseEntity.badRequest();
		}
		return ResponseEntity.ok(loginService.getOnlinePlayersList(json));
	}

	@RequestMapping(value = "setOpponentPlayer", method = RequestMethod.POST)
	public ResponseEntity<?> setOpponentPlayer(HttpServletRequest request, @Valid @RequestBody SetOpponentPlayer jso, BindingResult result) {
		if (result.hasErrors()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result.getAllErrors());
		}
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		JwtUser user = (JwtUser) userDetails;
		Integer userId = jwtTokenUtil.getUserIdFromToken(token);
		jso.setUser_id(userId);
		String json;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(jso);
		} catch (IOException e) {
			return (ResponseEntity<?>) ResponseEntity.badRequest();
		}
		return ResponseEntity.ok(loginService.setOpponentPlayer(json));
	}
	
	@RequestMapping(value = "getCurrentGameStatus", method = RequestMethod.POST)
	public ResponseEntity<?> getCurrentGameStatus(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		JwtUser user = (JwtUser) userDetails;
		Integer userId = jwtTokenUtil.getUserIdFromToken(token);
		PageDataDTO dto = new PageDataDTO();
		dto.setUser_id(userId);
		String json;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(dto);
		} catch (IOException e) {
			return (ResponseEntity<?>) ResponseEntity.badRequest();
		}
		return ResponseEntity.ok(loginService.getCurrentStatus(json));
	}
	
	@RequestMapping(value = "playGame", method = RequestMethod.POST)
	public ResponseEntity<?> playGame(HttpServletRequest request, @Valid @RequestBody PlayGame jso, BindingResult result) {
		if (result.hasErrors()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result.getAllErrors());
		}
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		JwtUser user = (JwtUser) userDetails;
		Integer userId = jwtTokenUtil.getUserIdFromToken(token);
		jso.setUser_id(userId);
		String json;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(jso);
		} catch (IOException e) {
			return (ResponseEntity<?>) ResponseEntity.badRequest();
		}
		return ResponseEntity.ok(loginService.play(json));
	}
	
	@RequestMapping(value = "quitGame", method = RequestMethod.POST)
	public ResponseEntity<?> quitGame(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		JwtUser user = (JwtUser) userDetails;
		Integer userId = jwtTokenUtil.getUserIdFromToken(token);
		PageDataDTO jso = new PageDataDTO();
		jso.setUser_id(userId);
		String json;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(jso);
		} catch (IOException e) {
			return (ResponseEntity<?>) ResponseEntity.badRequest();
		}
		return ResponseEntity.ok(loginService.quitGame(json));
	}
}