package com.connect_4.model.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}