package com.connect_4.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.connect_4.config.CorsProperties.Cors;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@ConditionalOnProperty(name = "application.cors.allowed-origins")
public class CorsFilter extends OncePerRequestFilter {

	protected CorsProperties properties;

	@Autowired
	public void setProperties(CorsProperties properties) {
		this.properties = properties;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		Cors cors = properties.getCors();
		String origin = request.getHeader("Origin");
		response.setHeader("Access-Control-Allow-Origin", ArrayUtils.contains(cors.getAllowedOrigins(), origin) ? // if
				origin : properties.getApplicationUrl());
		response.setHeader("Access-Control-Allow-Methods", StringUtils.join(cors.getAllowedMethods(), ","));
		response.setHeader("Access-Control-Allow-Headers", StringUtils.join(cors.getAllowedHeaders(), ","));
		response.setHeader("Access-Control-Expose-Headers", StringUtils.join(cors.getExposedHeaders(), ","));
		response.setHeader("Access-Control-Max-Age", Long.toString(cors.getMaxAge()));
		response.setHeader("Access-Control-Allow-Credentials", "true");
		if (!request.getMethod().equals("OPTIONS"))
			filterChain.doFilter(request, response);
	}
}