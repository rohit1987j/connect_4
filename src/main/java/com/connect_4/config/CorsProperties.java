package com.connect_4.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "application")
public class CorsProperties {

	private String applicationUrl = "http://localhost:8087";

	private Cors cors = new Cors();

	public Cors getCors() {
		return cors;
	}

	public void setCors(Cors cors) {
		this.cors = cors;
	}

	public String getApplicationUrl() {
		return applicationUrl;
	}

	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}
	public static class Cors {

		private String[] allowedOrigins;

		private String[] allowedMethods = { "GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "OPTIONS", "PATCH" };

		private String[] allowedHeaders = { "Accept", "Accept-Encoding", "Accept-Language", "Cache-Control",
				"Connection", "Content-Length", "Content-Type", "Cookie", "Host", "Origin", "Pragma", "Referer",
				"User-Agent", "x-requested-with", "Authorization"};

		private String[] exposedHeaders = { "Cache-Control", "Connection", "Content-Type", "Date", "Expires", "Pragma",
				"Server", "Set-Cookie", "Transfer-Encoding", "X-Content-Type-Options", "X-XSS-Protection",
				"X-Frame-Options", "X-Application-Context" };

		private long maxAge = 3600L;

		public String[] getAllowedOrigins() {
			return allowedOrigins;
		}

		public void setAllowedOrigins(String[] allowedOrigins) {
			this.allowedOrigins = allowedOrigins;
		}

		public String[] getAllowedMethods() {
			return allowedMethods;
		}

		public void setAllowedMethods(String[] allowedMethods) {
			this.allowedMethods = allowedMethods;
		}

		public String[] getAllowedHeaders() {
			return allowedHeaders;
		}

		public void setAllowedHeaders(String[] allowedHeaders) {
			this.allowedHeaders = allowedHeaders;
		}

		public String[] getExposedHeaders() {
			return exposedHeaders;
		}

		public void setExposedHeaders(String[] exposedHeaders) {
			this.exposedHeaders = exposedHeaders;
		}

		public long getMaxAge() {
			return maxAge;
		}

		public void setMaxAge(long maxAge) {
			this.maxAge = maxAge;
		}

	}
}