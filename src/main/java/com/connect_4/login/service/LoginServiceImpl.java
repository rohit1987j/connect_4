package com.connect_4.login.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.connect_4.login.dao.service.LoginDaoService;
import com.connect_4.login.dao.sp.SPGetCurrentStatus;
import com.connect_4.login.dao.sp.SPGetOnlinePlayerList;
import com.connect_4.login.dao.sp.SPPlayGame;
import com.connect_4.login.dao.sp.SPQuitGame;
import com.connect_4.login.dao.sp.SPSetOpponentPlayer;
import com.connect_4.login.dao.sp.SPSignUpProcedure;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginDaoService loginDaoService;

	@Override
	public SPSignUpProcedure signUp(String json) {
		return loginDaoService.signUp(json);
	}

	@Override
	public SPGetOnlinePlayerList getOnlinePlayersList(String json) {
		return loginDaoService.getOnlinePlayersList(json);
		}

	@Override
	public SPSetOpponentPlayer setOpponentPlayer(String json) {
		return loginDaoService.setOpponentPlayer(json);
	}

	@Override
	public SPGetCurrentStatus getCurrentStatus(String json) {
		return loginDaoService.getCurrentStatus(json);
	}

	@Override
	public SPQuitGame quitGame(String json) {
		return loginDaoService.quitGame(json);
	}

	@Override
	public SPPlayGame play(String json) {
		return loginDaoService.play(json);
	}

	@Override
	public void updateTableOnAction(String json) {
		 loginDaoService.updateTableOnAction(json);
	}
}