package com.connect_4.login.dao.service;

import com.connect_4.login.dao.sp.SPGetCurrentStatus;
import com.connect_4.login.dao.sp.SPGetOnlinePlayerList;
import com.connect_4.login.dao.sp.SPPlayGame;
import com.connect_4.login.dao.sp.SPQuitGame;
import com.connect_4.login.dao.sp.SPSetOpponentPlayer;
import com.connect_4.login.dao.sp.SPSignUpProcedure;

public interface LoginDaoService {

	public SPSignUpProcedure signUp(String json);
	
	public void updateTableOnAction(String json);

	public SPGetOnlinePlayerList getOnlinePlayersList(String json);

	public SPSetOpponentPlayer setOpponentPlayer(String json);

	public SPGetCurrentStatus getCurrentStatus(String json);
	
	public SPQuitGame quitGame(String json);
	
	public SPPlayGame play(String json);
}