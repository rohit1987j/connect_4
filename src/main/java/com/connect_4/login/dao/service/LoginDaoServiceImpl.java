package com.connect_4.login.dao.service;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.connect_4.login.dao.sp.SPGetCurrentStatus;
import com.connect_4.login.dao.sp.SPGetOnlinePlayerList;
import com.connect_4.login.dao.sp.SPPlayGame;
import com.connect_4.login.dao.sp.SPQuitGame;
import com.connect_4.login.dao.sp.SPSetOpponentPlayer;
import com.connect_4.login.dao.sp.SPSignUpProcedure;

@Service
@Transactional
public class LoginDaoServiceImpl implements LoginDaoService {
	
	private static final String DATA = "data";
	private static final String SIGN_UP = "call_sign_up_procedure";
	private static final String UPDATE_TABLE_ON_ACTION = "call_update_table_on_action_procedure";
	private static final String GET_ONLINE_PLAYER_LIST = "call_get_online_player_list_procedure";
	private static final String SET_OPPONENT_PLAYER = "call_set_opponent_player_procedure";
	private static final String PLAY_GAME = "call_play_game_procedure";
	private static final String QUIT_GAME = "call_quit_game_procedure";
	private static final String GET_CURRENT_GAME_STATUS = "call_get_current_status_procedure";

	@Autowired
	SessionFactory sessionFactory;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public SPSignUpProcedure signUp(String json) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(DATA, json);
		return (SPSignUpProcedure) getCurrentSession().getNamedQuery(SIGN_UP).setProperties(map).list().get(0);
	}

	@Override
	public SPGetOnlinePlayerList getOnlinePlayersList(String json) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(DATA, json);
		return (SPGetOnlinePlayerList)getCurrentSession().getNamedQuery(GET_ONLINE_PLAYER_LIST).setProperties(map).list().get(0);
	}

	@Override
	public SPSetOpponentPlayer setOpponentPlayer(String json) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(DATA, json);
		return (SPSetOpponentPlayer)getCurrentSession().getNamedQuery(SET_OPPONENT_PLAYER).setProperties(map).list().get(0);
	}

	@Override
	public SPGetCurrentStatus getCurrentStatus(String json) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(DATA, json);
		return (SPGetCurrentStatus)getCurrentSession().getNamedQuery(GET_CURRENT_GAME_STATUS).setProperties(map).list().get(0);
	}

	@Override
	public SPQuitGame quitGame(String json) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(DATA, json);
		return (SPQuitGame)getCurrentSession().getNamedQuery(QUIT_GAME).setProperties(map).list().get(0);
	}

	@Override
	public SPPlayGame play(String json) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(DATA, json);
		return (SPPlayGame)getCurrentSession().getNamedQuery(PLAY_GAME).setProperties(map).list().get(0);
	}

	@Override
	public void updateTableOnAction(String json) {
		Map<String, String> map = new HashMap<String, String>();
		map.put(DATA, json);
		getCurrentSession().getNamedQuery(UPDATE_TABLE_ON_ACTION).setProperties(map).list();
	}
}