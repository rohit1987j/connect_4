package com.connect_4.login.dao.sp;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name = "update_table_on_action", entities = @EntityResult(entityClass = SPUpdateTableOnAction.class))
@NamedNativeQueries({
	@NamedNativeQuery(name = "call_update_table_on_action_procedure", query = "select * from connect_4_db.update_table_on_action(:data)", resultSetMapping = "update_table_on_action") })
public class SPUpdateTableOnAction {
	private int idNum;
	private String message;
	private String data;
	private boolean success;

	@Id
	public int getIdNum() {
		return idNum;
	}

	public void setIdNum(int idNum) {
		this.idNum = idNum;
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}