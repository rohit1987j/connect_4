package com.connect_4.login.dao.sp;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name = "get_online_player_list", entities = @EntityResult(entityClass = SPGetOnlinePlayerList.class))
@NamedNativeQueries({
		@NamedNativeQuery(name = "call_get_online_player_list_procedure", query = "select * from connect_4_db.get_online_player_list(:data)", resultSetMapping = "get_online_player_list") })
public class SPGetOnlinePlayerList {
	private int idNum;
	private String message;
	private boolean success;
	private String data;

	@Id
	public int getIdNum() {
		return idNum;
	}

	public void setIdNum(int idNum) {
		this.idNum = idNum;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}