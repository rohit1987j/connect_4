package com.connect_4.login.dao.sp;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name = "quit_game", entities = @EntityResult(entityClass = SPQuitGame.class))
@NamedNativeQueries({
		@NamedNativeQuery(name = "call_quit_game_procedure", query = "select * from connect_4_db.quit_game(:data)", resultSetMapping = "quit_game") })
public class SPQuitGame {
	private int idNum;
	private String message;
	private boolean success;
	private String data;

	@Id
	public int getIdNum() {
		return idNum;
	}

	public void setIdNum(int idNum) {
		this.idNum = idNum;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}