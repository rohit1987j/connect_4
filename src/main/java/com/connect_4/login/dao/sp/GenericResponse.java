
package com.connect_4.login.dao.sp;

public class GenericResponse  {
	private int id_num;
	private boolean success;
	private String message;
	private String data;


	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getId_num() {
		return id_num;
	}

	public void setId_num(int idNum) {
		this.id_num = idNum;
	}
}