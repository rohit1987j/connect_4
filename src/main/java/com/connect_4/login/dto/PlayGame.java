package com.connect_4.login.dto;

import javax.validation.constraints.Min;

public class PlayGame {
	private int user_id;
	
	@Min(1)
	private int column_id;
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getColumn_id() {
		return column_id;
	}
	public void setColumn_id(int column_id) {
		this.column_id = column_id;
	}	
}