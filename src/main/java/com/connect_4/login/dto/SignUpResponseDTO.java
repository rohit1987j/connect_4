package com.connect_4.login.dto;

import java.io.Serializable;

public class SignUpResponseDTO implements Serializable {
	private boolean success;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}