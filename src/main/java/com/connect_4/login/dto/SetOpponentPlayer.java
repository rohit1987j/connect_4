package com.connect_4.login.dto;

import javax.validation.constraints.Min;

public class SetOpponentPlayer {
	private int user_id;
	
	@Min(1)
	private int opponent_player_id;
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getOpponent_player_id() {
		return opponent_player_id;
	}
	public void setOpponent_player_id(int opponent_player_id) {
		this.opponent_player_id = opponent_player_id;
	}	
}