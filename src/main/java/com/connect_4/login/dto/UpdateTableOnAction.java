package com.connect_4.login.dto;

public class UpdateTableOnAction {
	private int user_id;
	private String action;

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}	
}