--
-- rds_superuserQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

-- Started on 2017-01-05 22:27:15 IST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE connect_4;
--
-- TOC entry 2437 (class 1262 OID 19788)
-- Name: connect_4; Type: DATABASE; Schema: -; Owner: rds_superuser
--

CREATE DATABASE connect_4 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE connect_4 OWNER TO rds_superuser;

--\connect connect_4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 19789)
-- Name: connect_4_db; Type: SCHEMA; Schema: -; Owner: rds_superuser
--

CREATE SCHEMA connect_4_db;


ALTER SCHEMA connect_4_db OWNER TO rds_superuser;

--
-- TOC entry 1 (class 3079 OID 12623)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

--CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2438 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

--COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = connect_4_db, pg_catalog;

--
-- TOC entry 208 (class 1255 OID 19916)
-- Name: get_current_status(character varying); Type: FUNCTION; Schema: connect_4_db; Owner: rds_superuser
--

CREATE FUNCTION get_current_status(in_data character varying) RETURNS TABLE(idnum bigint, success boolean, message character varying, data character varying)
    LANGUAGE plpgsql
    AS $$
	declare
		--select * from connect_4_db.get_current_status('{"user_id": 2}');

		var_message character varying = '';
		var_data character varying = '';
		var_record record;
		var_record_2 record;
		var_user_id bigint = in_data::json->>'user_id';
		var_success boolean = true;
		var_last_user bigint = -1;
		var_next_user bigint = -1;
		var_next_username character varying = '';
	begin
		select * into var_record from game where (status = 'playing' and (user_id_1 = var_user_id or user_id_2 = var_user_id));
		
		if (var_record.id > 0) then
			select cast(json_agg(row) as character varying) from (
			select game_history.id, "user".username, game_history.player_time_stamp, game_history.row_id,
			game_history.column_id from game_history inner join "user" on "user".id = game_history.user_id
			order by player_time_stamp desc)  as row
			into var_data;
			select user_id from game_history into var_last_user
			where game_id = var_record.id order by player_time_stamp desc limit 1;
			if (var_record.user_id_1 = var_last_user) then
				var_next_user = var_record.user_id_2;
			else 
				var_next_user = var_record.user_id_1;
			end if;

			select username into var_next_username from "user" where id = var_next_user;

			var_message = 'Next chance is of player: '|| var_next_username;
		else
			var_message = 'You are not playing any game';
		end if;
		return query select var_user_id, var_success, var_message, var_data;
	end;
$$;


ALTER FUNCTION connect_4_db.get_current_status(in_data character varying) OWNER TO rds_superuser;

--
-- TOC entry 205 (class 1255 OID 19917)
-- Name: get_online_player_list(character varying); Type: FUNCTION; Schema: connect_4_db; Owner: rds_superuser
--

CREATE FUNCTION get_online_player_list(in_data character varying) RETURNS TABLE(idnum bigint, success boolean, message character varying, data character varying)
    LANGUAGE plpgsql
    AS $$
	declare
		var_success boolean = true;
		var_message character varying = 'Success';
		var_data character varying = '';
		var_user_id bigint = in_data::json->>'user_id';
		var_last_login_timestamp timestamp without time zone = null;
		var_last_logout_timestamp timestamp without time zone = null;
	begin
		select cast(json_agg(row) as character varying) from (
		select distinct "user".id as user_id, "user".firstname, "user".username from login_history
		inner join "user" on login_history.user_id = "user".id
		where (action = 'login' and ("user".id != var_user_id)))
		 as row into var_data;
		
		return query select 1::bigint, var_success, var_message, var_data;
	end;
$$;


ALTER FUNCTION connect_4_db.get_online_player_list(in_data character varying) OWNER TO rds_superuser;

--
-- TOC entry 209 (class 1255 OID 19918)
-- Name: play_game(character varying); Type: FUNCTION; Schema: connect_4_db; Owner: rds_superuser
--

CREATE FUNCTION play_game(in_data character varying) RETURNS TABLE(idnum bigint, success boolean, message character varying, data character varying)
    LANGUAGE plpgsql
    AS $$
declare
var_success boolean = true;
var_message character varying = '';
var_user_id bigint = in_data::json->>'user_id';
var_column_id bigint = in_data::json->>'column_id';
var_record record;
var_value bigint = -1;
var_count bigint = -1;
var_winner bigint = -1;
var_row bigint = -1;
var_column bigint = -1;
var_updated int = 0;
var_last_user bigint = -1;
var_next_user bigint = -1;
var_next_username character varying = '';
begin	
	select * from game into var_record where status = 'playing' and (user_id_1 = var_user_id or user_id_2 = var_user_id);
	
	select user_id from game_history into var_last_user where game_id = var_record.id order by player_time_stamp desc limit 1;
	if (var_last_user is null) then
		var_next_user = var_user_id;
	elsif (var_record.user_id_1 = var_last_user) then
		var_next_user = var_record.user_id_2;
	else
		var_next_user = var_record.user_id_1;
	end if;
	raise notice '2222  % --% -- % ', var_last_user, var_next_user, var_user_id;
	if (var_last_user is null or var_next_user is null or var_last_user != var_user_id) then
		if (var_record.id is not null and (var_record.winner_id is null or var_record.winner_id < 1)) then
			if(var_column_id < 1 or  var_column_id > 7) then
				var_message = 'Column should be between 1 and 7';
				var_success = false;
			else
				select var_record.matrix[1][var_column_id] into var_value;
				raise notice '1. var_value = %', var_value;
				if (var_value > 0) then
					var_message = 'Column is full please select some other column.';
					var_success = false;
				else
				--else here means means that space is there in the column

					for var_row IN 1..7 loop
						if (var_record.matrix[var_row][var_column_id]> 0) then
							update game set matrix[var_row-1][var_column_id] = var_user_id;
							insert into game_history(user_id, game_id, player_time_stamp, row_id, column_id) values
							(var_user_id, var_record.id, now(), var_row-1, var_column_id);
							var_updated = 1;
							var_message = 'Success';
							exit;
						end if;
					end loop;
					if (var_updated = 0) then
						update game set matrix[7][var_column_id] = var_user_id;
						insert into game_history(user_id, game_id, player_time_stamp, row_id, column_id) values
							(var_user_id, var_record.id, now(), 7, var_column_id);
						var_message = 'Success';
						var_updated = 1;
					end if;
				end if;

				if (var_winner< 1) then
					-- get winner in rows
					for var_row IN 1..7 loop
						var_count = 0;
						for j IN 2..7 loop
							if (var_record.matrix[var_row][var_column] > 0 and 
							(var_record.matrix[var_row][var_column] = var_record.matrix[var_row][var_column-1])) then
								var_count = var_count + 1;
							else
								var_count = 1;
							end if;

							if (var_count >= 4) then
							    var_winner = var_user_id;
							    --var_winner = var_record.matrix[var_row][var_column];
							    update game set status = 'complete', winner_id = var_winner
							    where id = var_record.id;
							    var_message = 'Congratulations, you won.';
							    exit;
							end if;
						end loop;
					end loop;
				end if;

				-- get winner in columns
				if (var_winner< 1) then
					for var_column IN 1..7 loop
						var_count = 0;
						for var_row IN 2..7 loop
							if (var_record.matrix[var_row][var_column] > 0 and
							 (var_record.matrix[var_row][var_column] = var_record.matrix[var_row-1][var_column])) then
								var_count = var_count + 1;
							else
								var_count = 1;
							end if;

							if (var_count >= 4) then
							    var_winner = var_user_id;
							    --var_winner = var_record.matrix[var_row][var_column];
							    update game set status = 'complete', winner_id = var_winner
							    where id = var_record.id;
							    var_message = 'Congratulations, you won.';
							    exit;
							end if;
						end loop;
					end loop;
				end if;

				-- first diagonal
				if (var_winner< 1) then
					for var_column IN 1..7 loop
						var_count = 0;
						for var_row IN 2..7 loop
							if (var_column + var_row >=7) then
								exit;
							else
								if (var_record.matrix[var_row][var_row+var_column] > 0 and 
								var_record.matrix[var_row-1][var_row+var_column-1] = 
								var_record.matrix[var_row][var_row+var_column]) then
									var_count = var_count + 1;
								else
									var_count = 1;
								end if;
							end if;
							if (var_count >=4 ) then
								var_winner = var_user_id;
								--var_winner = var_record.matrix[var_row][var_column+var_row];
								update game set status = 'complete', winner_id = var_winner
								where id = var_record.id;
								var_message = 'Congratulations, you won.';
								--var_winner = var_record.matrix[var_row][var_column+var_row];
							end if;
						end loop;
					end loop;
				end if;

				if (var_winner< 1) then
					for var_row IN 1..7 loop
						var_count = 0;
						for var_column IN 2..7 loop
							if (var_row + var_column >=7) then
								exit;
							else
								if (var_record.matrix[var_row+var_column][var_column] > 0 and 
								var_record.matrix[var_row+var_column-1][var_column-1] = 
								var_record.matrix[var_row+var_column][var_column]) then
									var_count = var_count + 1;
								else
									var_count = 1;
								end if;
							end if;
							if (var_count >=4 ) then
								var_winner = var_user_id;
								--var_winner = var_record.matrix[var_row+var_column][var_column];
								update game set status = 'complete', winner_id = var_winner
								where id = var_record.id;
								var_message = 'Congratulations, you won.';
								--var_winner = var_record.matrix[var_row+var_column][var_column];
							end if;
						end loop;
					end loop;
				end if;

				if (var_winner< 1) then
					for var_column IN 1..7 loop
						var_count = 0;
						for var_row IN 2..7 loop
							if (var_column - var_row < 0) then
								exit;
							else
								if (var_record.matrix[var_row][var_column-var_row] > 0 and 
								var_record.matrix[var_row-1][var_column-var_row+1] = 
								var_record.matrix[var_row][var_column-var_row]) then
									var_count = var_count + 1;
								else
									var_count = 1;
								end if;
							end if;
							if (var_count >=4 ) then
								var_winner = var_user_id;
								--var_winner = var_record.matrix[var_row][var_column-var_row];
								update game set status = 'complete', winner_id = var_winner
								where id = var_record.id;
								var_message = 'Congratulations, you won.';
								--var_winner = var_record.matrix[var_row][var_column-var_row];
							end if;
						end loop;
					end loop;
				end if;

				if (var_winner< 1) then
					for var_row IN 1..7 loop
						var_count = 0;
						for var_column IN REVERSE 6..1 loop
							if (var_column - var_row < 0) then
								exit;
							else
								if (var_record.matrix[var_column-var_row][var_column] > 0 and 
								var_record.matrix[var_column-var_row-1][var_column+1] = 
								var_record.matrix[var_column-var_row][var_column]) then
									var_count = var_count + 1;
								else
									var_count = 1;
								end if;
							end if;
							if (var_count >=4 ) then
								var_winner = var_user_id;
								--var_winner = var_record.matrix[var_column-var_row][var_column];
								update game set status = 'complete', winner_id = var_winner
								where id = var_record.id;
								var_message = 'Congratulations, you won.';
								--var_winner = var_record.matrix[var_column-var_row][var_column];
							end if;
						end loop;
					end loop;
				end if;
			end if;
		else
			var_success = false;
			var_message = 'Sorry you lost. Game over. Better luck next time.';
		end if;
	else
		var_success = false;
		select username into var_next_username from "user" where id = var_next_user;
		var_message = 'Next chance is of player: '|| var_next_username;		
	end if;
	return query select var_user_id, var_success, var_message, var_message;
end;
$$;


ALTER FUNCTION connect_4_db.play_game(in_data character varying) OWNER TO rds_superuser;

--
-- TOC entry 204 (class 1255 OID 19915)
-- Name: quit_game(character varying); Type: FUNCTION; Schema: connect_4_db; Owner: rds_superuser
--

CREATE FUNCTION quit_game(in_data character varying) RETURNS TABLE(idnum bigint, success boolean, message character varying, data character varying)
    LANGUAGE plpgsql
    AS $$
	declare
		var_user_id bigint = in_data::json->>'user_id';
		var_record record;
		var_success boolean = false;
		var_message character varying = 'You are not playing any game.';
		var_data character varying = '';
		var_winner_id bigint = -1;
	begin
		select * from game into var_record where (status = 'playing' and (user_id_1 = var_user_id
		or user_id_2 = var_user_id));
		--var_record is not null and 
		if (var_record.id > 0) then
			if (var_record.user_id_1 = var_user_id) then
				var_winner_id = var_record.user_id_2;
			else
				var_winner_id = var_record.user_id_1;
			end if;
			var_success = true;
			var_message = 'Success';
			update game set status = 'quit', winner_id = var_winner_id
			where id = var_record.id;
		end if;
		return query select var_user_id, var_success, var_message, var_data;
	end;
$$;


ALTER FUNCTION connect_4_db.quit_game(in_data character varying) OWNER TO rds_superuser;

--
-- TOC entry 210 (class 1255 OID 19914)
-- Name: set_opponent_player(character varying); Type: FUNCTION; Schema: connect_4_db; Owner: rds_superuser
--

CREATE FUNCTION set_opponent_player(in_data character varying) RETURNS TABLE(idnum bigint, success boolean, message character varying, data character varying)
    LANGUAGE plpgsql
    AS $$
	declare
		var_success boolean = false;
		var_message character varying = '';
		var_data character varying = '';
		var_user_id bigint = in_data::json->>'user_id';
		var_opponent_id bigint = in_data::json->>'opponent_player_id';
	begin
		if ((select count(id) from "user" where "user".id = var_opponent_id) < 1) then
			var_message = 'Invalid Player Id.';
		elsif((select count(id) from login_history where (user_id = var_opponent_id)) < 1) then
			var_message = 'Player is not online.';
		elsif ((select count(id) from game where ((user_id_1 = var_user_id or user_id_2 = var_user_id) and status = 'playing')) > 0) then
			var_message = 'You are already playing match, please first quit current match.';
		elsif((select count(id) from game where ((user_id_1 = var_opponent_id or user_id_2 = var_opponent_id)  and status = 'playing')) > 0) then
			var_message = 'Your opponent is already playing match, please select some other opponent.';
		else
			insert into game(user_id_1, user_id_2, matrix, status) 
			values(var_user_id, var_opponent_id,
			'{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}}',
			'playing');
			var_success = true;
			var_message = 'Game has started.';
		end if;
		return query select var_user_id, var_success, var_message, var_data;
	end;
$$;


ALTER FUNCTION connect_4_db.set_opponent_player(in_data character varying) OWNER TO rds_superuser;

--
-- TOC entry 207 (class 1255 OID 19919)
-- Name: sign_up(character varying); Type: FUNCTION; Schema: connect_4_db; Owner: rds_superuser
--

CREATE FUNCTION sign_up(in_data character varying) RETURNS TABLE(idnum bigint, success boolean, message character varying, data character varying)
    LANGUAGE plpgsql
    AS $$

declare

var_data character varying = '';

var_message character varying = '';

var_success boolean = false;

var_username character varying = in_data::json->>'username';

var_firstname character varying = in_data::json->>'firstname';

var_lastname character varying = in_data::json->>'lastname';

var_email character varying = in_data::json->>'email';

var_password character varying = in_data::json->>'password';

var_new_user_id bigint = 0;

BEGIN

	if ((select count("user".id) from "user" where lower(username) = lower(var_username)) > 0) then
		var_message = 'User with this username already exists.';
	elsif ((select count("user".id) from "user" where lower(email) = lower(var_email)) > 0) then
		var_message = 'User with this email already exists.';
	else
		insert into "user"(email, username, firstname, lastname, enabled, password, lastpasswordresetdate) 

		values(var_email, var_username, var_firstname, var_lastname, true, var_password, now());

		SELECT currval('user_id_seq') into var_new_user_id;

		var_message = 'Sign Up Successfull';
		var_success = true;

		insert into user_authority(user_id, authority_id) values(var_new_user_id, 
		(select id from authority where name = 'ROLE_ADMIN'));

	end if;

	return query select var_new_user_id, var_success, var_message, var_data;
end;

$$;


ALTER FUNCTION connect_4_db.sign_up(in_data character varying) OWNER TO rds_superuser;

--
-- TOC entry 206 (class 1255 OID 19912)
-- Name: update_table_on_action(character varying); Type: FUNCTION; Schema: connect_4_db; Owner: rds_superuser
--

CREATE FUNCTION update_table_on_action(in_data character varying) RETURNS TABLE(idnum bigint, success boolean, message character varying, data character varying)
    LANGUAGE plpgsql
    AS $$
		declare
			var_message character varying = '';
			var_data character varying = '';
			var_success boolean = false;
			var_user_id bigint = in_data::json->>'user_id';
			var_action character varying = in_data::json->>'action';
			var_last_action character varying = '';
		begin
			select action from login_history into var_last_action
			where user_id = var_user_id order by id desc limit 1 ;
			if (var_last_action is null) then
				if (var_action = 'login') then
					insert into login_history(user_id, timestamp, action) values(var_user_id, now(), 'login');
				end if;
			elsif(var_last_action = 'login' and var_action = 'login') then
				insert into login_history(user_id, timestamp, action) values(var_user_id, now(), 'logout');
				insert into login_history(user_id, timestamp, action) values(var_user_id, now(), 'login');
			elsif(var_last_action = 'logout' and var_action = 'login') then
				insert into login_history(user_id, timestamp, action) values(var_user_id, now(), 'login');
			elsif(var_last_action = 'login' and var_action = 'logout') then
				insert into login_history(user_id, timestamp, action) values(var_user_id, now(), 'logout');
			end if;
			return query select var_user_id, var_success, var_message, var_data;
		end;
	$$;


ALTER FUNCTION connect_4_db.update_table_on_action(in_data character varying) OWNER TO rds_superuser;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 19874)
-- Name: authority; Type: TABLE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE TABLE authority (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE authority OWNER TO rds_superuser;

--
-- TOC entry 186 (class 1259 OID 19872)
-- Name: authority_id_seq; Type: SEQUENCE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE SEQUENCE authority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authority_id_seq OWNER TO rds_superuser;

--
-- TOC entry 2439 (class 0 OID 0)
-- Dependencies: 186
-- Name: authority_id_seq; Type: SEQUENCE OWNED BY; Schema: connect_4_db; Owner: rds_superuser
--

ALTER SEQUENCE authority_id_seq OWNED BY authority.id;


--
-- TOC entry 185 (class 1259 OID 19854)
-- Name: game; Type: TABLE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE TABLE game (
    id bigint NOT NULL,
    user_id_1 bigint NOT NULL,
    user_id_2 bigint NOT NULL,
    status character varying NOT NULL,
    matrix bigint[],
    winner_id bigint
);


ALTER TABLE game OWNER TO rds_superuser;

--
-- TOC entry 189 (class 1259 OID 19887)
-- Name: game_history; Type: TABLE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE TABLE game_history (
    id bigint NOT NULL,
    user_id bigint,
    game_id bigint,
    player_time_stamp timestamp without time zone,
    row_id integer,
    column_id integer
);


ALTER TABLE game_history OWNER TO rds_superuser;

--
-- TOC entry 184 (class 1259 OID 19852)
-- Name: game_id_seq; Type: SEQUENCE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE SEQUENCE game_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE game_id_seq OWNER TO rds_superuser;

--
-- TOC entry 2440 (class 0 OID 0)
-- Dependencies: 184
-- Name: game_id_seq; Type: SEQUENCE OWNED BY; Schema: connect_4_db; Owner: rds_superuser
--

ALTER SEQUENCE game_id_seq OWNED BY game.id;


--
-- TOC entry 188 (class 1259 OID 19885)
-- Name: game_turns_id_seq; Type: SEQUENCE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE SEQUENCE game_turns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE game_turns_id_seq OWNER TO rds_superuser;

--
-- TOC entry 2441 (class 0 OID 0)
-- Dependencies: 188
-- Name: game_turns_id_seq; Type: SEQUENCE OWNED BY; Schema: connect_4_db; Owner: rds_superuser
--

ALTER SEQUENCE game_turns_id_seq OWNED BY game_history.id;


--
-- TOC entry 191 (class 1259 OID 19893)
-- Name: login_history; Type: TABLE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE TABLE login_history (
    id bigint NOT NULL,
    action character varying(50) NOT NULL,
    user_id bigint,
    "timestamp" time without time zone
);


ALTER TABLE login_history OWNER TO rds_superuser;

--
-- TOC entry 190 (class 1259 OID 19891)
-- Name: login_history_id_seq; Type: SEQUENCE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE SEQUENCE login_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE login_history_id_seq OWNER TO rds_superuser;

--
-- TOC entry 2442 (class 0 OID 0)
-- Dependencies: 190
-- Name: login_history_id_seq; Type: SEQUENCE OWNED BY; Schema: connect_4_db; Owner: rds_superuser
--

ALTER SEQUENCE login_history_id_seq OWNED BY login_history.id;


--
-- TOC entry 182 (class 1259 OID 19795)
-- Name: user; Type: TABLE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE TABLE "user" (
    id bigint NOT NULL,
    email character varying(50) NOT NULL,
    enabled boolean NOT NULL,
    firstname character varying(50) NOT NULL,
    lastpasswordresetdate timestamp without time zone NOT NULL,
    lastname character varying(50) NOT NULL,
    password character varying(100) NOT NULL,
    username character varying(50) NOT NULL
);


ALTER TABLE "user" OWNER TO rds_superuser;

--
-- TOC entry 183 (class 1259 OID 19811)
-- Name: user_authority; Type: TABLE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE TABLE user_authority (
    user_id bigint NOT NULL,
    authority_id bigint NOT NULL
);


ALTER TABLE user_authority OWNER TO rds_superuser;

--
-- TOC entry 181 (class 1259 OID 19793)
-- Name: user_id_seq; Type: SEQUENCE; Schema: connect_4_db; Owner: rds_superuser
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO rds_superuser;

--
-- TOC entry 2443 (class 0 OID 0)
-- Dependencies: 181
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: connect_4_db; Owner: rds_superuser
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- TOC entry 2285 (class 2604 OID 19877)
-- Name: id; Type: DEFAULT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY authority ALTER COLUMN id SET DEFAULT nextval('authority_id_seq'::regclass);


--
-- TOC entry 2284 (class 2604 OID 19857)
-- Name: id; Type: DEFAULT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game ALTER COLUMN id SET DEFAULT nextval('game_id_seq'::regclass);


--
-- TOC entry 2286 (class 2604 OID 19890)
-- Name: id; Type: DEFAULT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game_history ALTER COLUMN id SET DEFAULT nextval('game_turns_id_seq'::regclass);


--
-- TOC entry 2287 (class 2604 OID 19896)
-- Name: id; Type: DEFAULT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY login_history ALTER COLUMN id SET DEFAULT nextval('login_history_id_seq'::regclass);


--
-- TOC entry 2283 (class 2604 OID 19798)
-- Name: id; Type: DEFAULT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 2428 (class 0 OID 19874)
-- Dependencies: 187
-- Data for Name: authority; Type: TABLE DATA; Schema: connect_4_db; Owner: rds_superuser
--

INSERT INTO authority (id, name) VALUES (1, 'ROLE_ADMIN');


--
-- TOC entry 2444 (class 0 OID 0)
-- Dependencies: 186
-- Name: authority_id_seq; Type: SEQUENCE SET; Schema: connect_4_db; Owner: rds_superuser
--

SELECT pg_catalog.setval('authority_id_seq', 1, true);


--
-- TOC entry 2426 (class 0 OID 19854)
-- Dependencies: 185
-- Data for Name: game; Type: TABLE DATA; Schema: connect_4_db; Owner: rds_superuser
--

INSERT INTO game (id, user_id_1, user_id_2, status, matrix, winner_id) VALUES (10, 2, 3, 'quit', '{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{10,0,0,0,0,0,0},{10,0,0,2,0,0,0},{10,0,0,2,0,0,2},{10,0,0,2,0,0,2},{2,10,0,2,0,0,10}}', 3);
INSERT INTO game (id, user_id_1, user_id_2, status, matrix, winner_id) VALUES (7, 10, 2, 'quit', '{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{10,0,0,0,0,0,0},{10,0,0,2,0,0,0},{10,0,0,2,0,0,2},{10,0,0,2,0,0,2},{2,10,0,2,0,0,10}}', 2);
INSERT INTO game (id, user_id_1, user_id_2, status, matrix, winner_id) VALUES (8, 10, 3, 'quit', '{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{10,0,0,0,0,0,0},{10,0,0,2,0,0,0},{10,0,0,2,0,0,2},{10,0,0,2,0,0,2},{2,10,0,2,0,0,10}}', 3);
INSERT INTO game (id, user_id_1, user_id_2, status, matrix, winner_id) VALUES (9, 10, 2, 'quit', '{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{10,0,0,0,0,0,0},{10,0,0,2,0,0,0},{10,0,0,2,0,0,2},{10,0,0,2,0,0,2},{2,10,0,2,0,0,10}}', 10);
INSERT INTO game (id, user_id_1, user_id_2, status, matrix, winner_id) VALUES (11, 2, 10, 'complete', '{{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{10,0,0,0,0,0,0},{10,0,0,2,0,0,0},{10,0,0,2,0,0,2},{10,0,0,2,0,0,2},{2,10,0,2,0,0,10}}', 10);


--
-- TOC entry 2430 (class 0 OID 19887)
-- Dependencies: 189
-- Data for Name: game_history; Type: TABLE DATA; Schema: connect_4_db; Owner: rds_superuser
--

INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (14, 2, 11, '2017-01-05 21:28:11.774512', 7, 1);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (15, 10, 11, '2017-01-05 21:28:41.385554', 7, 7);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (16, 2, 11, '2017-01-05 21:29:47.227143', 6, 7);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (17, 10, 11, '2017-01-05 21:30:00.645156', 7, 2);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (18, 2, 11, '2017-01-05 21:30:17.016892', 5, 7);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (19, 10, 11, '2017-01-05 21:30:36.973776', 6, 1);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (20, 2, 11, '2017-01-05 21:30:38.993374', 7, 4);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (21, 10, 11, '2017-01-05 21:30:42.116821', 5, 1);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (22, 2, 11, '2017-01-05 21:30:43.622134', 6, 4);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (23, 10, 11, '2017-01-05 21:30:45.596374', 4, 1);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (24, 2, 11, '2017-01-05 21:30:47.181732', 5, 4);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (25, 10, 11, '2017-01-05 21:30:48.337404', 3, 1);
INSERT INTO game_history (id, user_id, game_id, player_time_stamp, row_id, column_id) VALUES (26, 2, 11, '2017-01-05 21:30:50.463825', 4, 4);


--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 184
-- Name: game_id_seq; Type: SEQUENCE SET; Schema: connect_4_db; Owner: rds_superuser
--

SELECT pg_catalog.setval('game_id_seq', 11, true);


--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 188
-- Name: game_turns_id_seq; Type: SEQUENCE SET; Schema: connect_4_db; Owner: rds_superuser
--

SELECT pg_catalog.setval('game_turns_id_seq', 26, true);


--
-- TOC entry 2432 (class 0 OID 19893)
-- Dependencies: 191
-- Data for Name: login_history; Type: TABLE DATA; Schema: connect_4_db; Owner: rds_superuser
--

INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (2, 'login', 2, '13:55:06.04329');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (3, 'login', 3, '13:55:09.634958');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (4, 'logout', 3, '13:55:15.554341');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (5, 'logout', 2, '15:57:26.14705');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (6, 'login', 2, '15:57:26.14705');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (7, 'login', 10, '20:39:40.975998');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (8, 'logout', 2, '20:40:04.079428');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (9, 'login', 2, '20:40:04.079428');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (10, 'logout', 2, '20:54:54.969505');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (11, 'login', 2, '20:54:54.969505');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (12, 'logout', 2, '21:09:09.273464');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (13, 'login', 2, '21:09:09.273464');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (14, 'logout', 10, '21:09:19.2552');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (15, 'login', 10, '21:09:19.2552');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (16, 'logout', 10, '21:20:24.669852');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (17, 'login', 10, '21:20:24.669852');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (18, 'logout', 2, '21:23:45.089685');
INSERT INTO login_history (id, action, user_id, "timestamp") VALUES (19, 'login', 2, '21:23:45.089685');


--
-- TOC entry 2447 (class 0 OID 0)
-- Dependencies: 190
-- Name: login_history_id_seq; Type: SEQUENCE SET; Schema: connect_4_db; Owner: rds_superuser
--

SELECT pg_catalog.setval('login_history_id_seq', 19, true);


--
-- TOC entry 2423 (class 0 OID 19795)
-- Dependencies: 182
-- Data for Name: user; Type: TABLE DATA; Schema: connect_4_db; Owner: rds_superuser
--

INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (2, 'rohit1987j@gmail.com', true, 'rohit', '2017-01-05 15:48:12.226711', 'jain', '$2a$08$v0p7ggugv/qqWscBSIAbneIjhEHw/Pcxb6Hx2Ho.s9EmluvV9Edzq', 'rohitjain');
INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (3, 'neerajsoni@gmail.com', true, 'neeraj', '2017-01-05 15:48:51.115112', 'soni', '$2a$08$v0p7ggugv/qqWscBSIAbneIjhEHw/Pcxb6Hx2Ho.s9EmluvV9Edzq', 'neerajsoni');
INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (4, 'uditaverma@gmail.com', true, 'udita', '2017-01-05 15:49:22.326722', 'verma', '$2a$08$v0p7ggugv/qqWscBSIAbneIjhEHw/Pcxb6Hx2Ho.s9EmluvV9Edzq', 'uditaverma');
INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (5, 'asdsad@sad.com', true, 'ssssss', '2017-01-05 17:43:32.515732', 'ssssss', '$2a$10$Tf1duAYq1VmSNNQeVDwiRupktiUXW9m92xIx7oufMTeg.3VZF8ZFi', 'ssssss');
INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (6, 'asdsadd@sad.com', true, 'ssssss', '2017-01-05 18:24:53.618421', 'ssssss', '$2a$10$6LuuLgNq0rvu4DxoD0dhH.CAihjfjUDiBhms86tjEaL6qadSQZDEe', 'ddssssss');
INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (7, 'akansha@gmail.com', true, 'akansha', '2017-01-05 20:04:55.130512', 'jain', '$2a$10$h.cIwBtslQVB0wJAGcmNAutOiUB9zQvwFhF1DTK.lsA.xOsRlawX2', 'akansha');
INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (8, 'akanssha@gmail.com', true, 'akanshas', '2017-01-05 20:11:30.494764', 'jain', '$2a$10$dOtIBOqgj.FiPOGADs8nuOQfdDAg5n5f0qVYO4N2Ryxn5VJMLnq92', 'akasnsha');
INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (9, 'jatin@gmail.com', true, 'akanshas', '2017-01-05 20:18:14.570671', 'jain', '$2a$10$0OiZg3Lt8ZiX4XTxmongme9v5lKxRx54o/HpUZvdIRQpa5GDBwLlu', 'jatin');
INSERT INTO "user" (id, email, enabled, firstname, lastpasswordresetdate, lastname, password, username) VALUES (10, 'neeraj@soni.com', true, 'neeraj', '2017-01-05 20:39:06.60872', 'soni', '$2a$10$UhQGat/rTwUe95YWjmvLBOP66OkbsyyWrgafU.2xEP34JR.yiu71u', 'nrsoni');


--
-- TOC entry 2424 (class 0 OID 19811)
-- Dependencies: 183
-- Data for Name: user_authority; Type: TABLE DATA; Schema: connect_4_db; Owner: rds_superuser
--

INSERT INTO user_authority (user_id, authority_id) VALUES (2, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (3, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (4, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (5, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (6, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (7, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (8, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (9, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (10, 1);


--
-- TOC entry 2448 (class 0 OID 0)
-- Dependencies: 181
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: connect_4_db; Owner: rds_superuser
--

SELECT pg_catalog.setval('user_id_seq', 10, true);


--
-- TOC entry 2295 (class 2606 OID 19879)
-- Name: authority_pkey; Type: CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY authority
    ADD CONSTRAINT authority_pkey PRIMARY KEY (id);


--
-- TOC entry 2297 (class 2606 OID 19938)
-- Name: game_history_pkey; Type: CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game_history
    ADD CONSTRAINT game_history_pkey PRIMARY KEY (id);


--
-- TOC entry 2293 (class 2606 OID 19921)
-- Name: game_pkey; Type: CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game
    ADD CONSTRAINT game_pkey PRIMARY KEY (id);


--
-- TOC entry 2299 (class 2606 OID 19898)
-- Name: login_history_pkey; Type: CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY login_history
    ADD CONSTRAINT login_history_pkey PRIMARY KEY (id);


--
-- TOC entry 2289 (class 2606 OID 19802)
-- Name: uk_sb8bbouer5wak8vyiiy4pf2bx; Type: CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT uk_sb8bbouer5wak8vyiiy4pf2bx UNIQUE (username);


--
-- TOC entry 2291 (class 2606 OID 19800)
-- Name: user_pkey; Type: CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2300 (class 2606 OID 19814)
-- Name: fk_5losscgu02yaej7prap7o6g5s; Type: FK CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY user_authority
    ADD CONSTRAINT fk_5losscgu02yaej7prap7o6g5s FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- TOC entry 2306 (class 2606 OID 19944)
-- Name: game_history_game_id_fkey; Type: FK CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game_history
    ADD CONSTRAINT game_history_game_id_fkey FOREIGN KEY (game_id) REFERENCES game(id);


--
-- TOC entry 2305 (class 2606 OID 19939)
-- Name: game_history_user_id_fkey; Type: FK CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game_history
    ADD CONSTRAINT game_history_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- TOC entry 2302 (class 2606 OID 19922)
-- Name: game_user_id_1_fkey; Type: FK CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game
    ADD CONSTRAINT game_user_id_1_fkey FOREIGN KEY (user_id_1) REFERENCES "user"(id);


--
-- TOC entry 2303 (class 2606 OID 19927)
-- Name: game_user_id_2_fkey; Type: FK CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game
    ADD CONSTRAINT game_user_id_2_fkey FOREIGN KEY (user_id_2) REFERENCES "user"(id);


--
-- TOC entry 2304 (class 2606 OID 19932)
-- Name: game_winner_id_fkey; Type: FK CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY game
    ADD CONSTRAINT game_winner_id_fkey FOREIGN KEY (winner_id) REFERENCES "user"(id);


--
-- TOC entry 2307 (class 2606 OID 19949)
-- Name: login_history_user_id_fkey; Type: FK CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY login_history
    ADD CONSTRAINT login_history_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- TOC entry 2301 (class 2606 OID 19954)
-- Name: user_authority_authority_id_fkey; Type: FK CONSTRAINT; Schema: connect_4_db; Owner: rds_superuser
--

ALTER TABLE ONLY user_authority
    ADD CONSTRAINT user_authority_authority_id_fkey FOREIGN KEY (authority_id) REFERENCES authority(id);


-- Completed on 2017-01-05 22:27:15 IST

--
-- rds_superuserQL database dump complete
--

